package com.microservice.orderservice.external.client;

import com.microservice.orderservice.payload.request.PaymentRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//If we are using Service registry then no need url
//If we are using FeignClient the no need @LoadBalanced
//The @FeignClient is designed to work with Ribbon for load balancing automatically. Ribbon is a client-side load balancer that integrates with Eureka for service discovery.
//@FeignClient(name = "PAYMENT-SERVICE", url = "http://localhost:8083/payment")
@FeignClient(name = "PAYMENT-SERVICE")
public interface PaymentService {
    @PostMapping("/payment")
    public ResponseEntity<Long> doPayment(@RequestBody PaymentRequest paymentRequest);
}
