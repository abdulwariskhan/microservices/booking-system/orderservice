package com.microservice.orderservice.external.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
//If we are using Service registry then no need url
//If we are using FeignClient the no need @LoadBalanced
//The @FeignClient is designed to work with Ribbon for load balancing automatically. Ribbon is a client-side load balancer that integrates with Eureka for service discovery.
//@FeignClient(name = "PRODUCT-SERVICE", url = "http://localhost:8081/product")
@FeignClient(name = "PRODUCT-SERVICE")
public interface ProductService {
    @PutMapping("/product/reduceQuantity/{id}")
    ResponseEntity<Void> reduceQuantity(
            @PathVariable("id") long productId,
            @RequestParam long quantity
    );
}
